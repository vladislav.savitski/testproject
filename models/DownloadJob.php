<?php


namespace app\models;


use app\models\Currency;
use SebastianBergmann\CodeCoverage\TestFixture\C;
use yii\base\BaseObject;


class DownloadJob extends BaseObject implements \yii\queue\JobInterface
{
    public $type;
    public $date;
    public $buy;
    public $sell;

    public function execute($queue)
    {
        $json = file_get_contents('http://blockchain.info/ticker');
        $data = json_decode($json, true);
        $currency = new Currency();
        $currency -> setIsNewRecord(true);
        $currency -> id = null;
        $currency -> type = 'AUD';
        $currency -> date = date("Y-m-d H:i:s");
        $currency -> buy = (float)$data['AUD']['buy'];
        $currency -> sell = (float)$data['AUD']['sell'];
        $currency -> save();
        $currency = new Currency();
        $currency -> setIsNewRecord(true);
        $currency -> id = null;
        $currency -> type = 'USD';
        $currency -> date = date("Y-m-d H:i:s");
        $currency -> buy = (float)$data['USD']['buy'];
        $currency -> sell = (float)$data['USD']['sell'];
        $currency -> save();
        $currency = new Currency();
        $currency -> setIsNewRecord(true);
        $currency -> id = null;
        $currency -> type = 'EUR';
        $currency -> date = date("Y-m-d H:i:s");
        $currency -> buy = (float)$data['EUR']['buy'];
        $currency -> sell = (float)$data['EUR']['sell'];
        $currency -> save();
    }
}