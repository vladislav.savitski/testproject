<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "currency".
 *
 * @property int $id
 * @property string $type
 * @property string $date
 * @property float $buy
 * @property float $sell
 */
class Currency extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'currency';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['type', 'date', 'buy', 'sell'], 'required'],
            [['id'], 'integer'],
            [['date'], 'safe'],
            [['buy', 'sell'], 'number'],
            [['type'], 'string', 'max' => 45],
            [['id'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'type' => 'Type',
            'date' => 'Date',
            'buy' => 'Buy',
            'sell' => 'Sell',
        ];
    }


}
