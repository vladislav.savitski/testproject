<?php

namespace app\controllers;

use app\models\DownloadJob;
use http\Exception;
use Yii;
use app\models\Currency;
use yii\data\ActiveDataProvider;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;
use yii\rest\ActiveController;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\web\Link;
use yii\web\Linkable;
use yii\helpers\Url;


class CurrencyController extends ActiveController
{
    public $modelClass = Currency::class;

    public function behaviors()
    {
        Yii::$app->queue->push(new DownloadJob());
        return [
            'contentNegotiator' => [
                'class' => \yii\filters\ContentNegotiator::class,
                'formatParam' => 'format',
                'formats' => [
                    'application/json' => \yii\web\Response::FORMAT_JSON,
                ],
            ],
        ];
    }


    public function actionSearch()
    {
        if (!empty($_GET)) {
            $model = new $this->modelClass;
            foreach ($_GET as $key => $value) {
                if (!$model->hasAttribute($key)) {
                    throw new \yii\web\HttpException(404, 'Invalid attribute:' . $key);
                }
            }
            try {
                $provider = new ActiveDataProvider([
                    'query' => $model->find()->where($_GET)->orderBy(['id' => SORT_DESC]),
                    'pagination' => false
                ]);
            } catch (Exception $ex) {
                throw new \yii\web\HttpException(500, 'Internal server error');
            }

            if ($provider->getCount() <= 0) {
                throw new \yii\web\HttpException(404, 'No entries found with this query string');
            } else {
                return $provider;
            }
        } else {
            throw new \yii\web\HttpException(400, 'There are no query string');
        }
    }

    public function actionDate()
    {
        var_dump($_GET);
        if(array_key_exists('earlier', $_GET)){
            return Currency::find()
                ->where(['<', 'date', $_GET['earlier']])
                ->orderBy('id')
                ->all();
        } else if(array_key_exists('later', $_GET)){
            return Currency::find()
                ->where(['>', 'date', $_GET['later']])
                ->orderBy('id')
                ->all();
        } else if (array_key_exists('day', $_GET)){
            return Currency::find()
                ->where(['<', 'date', $_GET['day']])
                ->orderBy('id')
                ->all();
        } else {
            return "error key";
        }
    }

    public function actionTime()
    {
        $data_start = $_GET['start'];
        $data_end = $_GET['end'];
        var_dump($data_start);
        return Currency::find()
            ->where(['between', 'date', $data_start, $data_end ])->all();
    }

    public function actionUsd()
    {
        return Currency::find()
            ->select(['date', 'buy'])->where(['type' => 'USD' ])->orderBy(['id' => SORT_DESC])->all();
    }

    public function actionEur()
    {
        return Currency::find()
            ->select(['date', 'buy'])->where(['type' => 'EUR' ])->orderBy(['id' => SORT_DESC])->all();
    }

    public function actionAud()
    {
        return Currency::find()
            ->select(['date', 'buy'])->where(['type' => 'AUD' ])->orderBy(['id' => SORT_DESC])->all();
    }
}
