<?php

/* @var $this yii\web\View */
use antkaz\vue\Vue;
use app\models\Currency;
use app\models\DownloadJob;
use yii\helpers\Html;
use \yii\web\JsExpression;
use antkaz\vue\VueAsset;

VueAsset::register($this);
$this->title = 'My Yii Application';

$currencyUsd = Currency::find()->select(['buy', 'date'])->where(['type' => 'USD'])
    ->limit(5)->orderBy(['id' => SORT_DESC])->all();
$currencyEur = Currency::find()->select(['buy', 'date'])->where(['type' => 'EUR'])
    ->limit(5)->orderBy(['id' => SORT_DESC])->all();
$currencyAud = Currency::find()->select(['buy', 'date'])->where(['type' => 'AUD'])
    ->limit(5)->orderBy(['id' => SORT_DESC])->all();
?>

<?php
try {
    $series = [
        [
            'name' => 'USD',
            'data' => [
                [$currencyUsd[0]['date'], $currencyUsd[0]['buy']],
                [$currencyUsd[1]['date'], $currencyUsd[1]['buy']],
                [$currencyUsd[2]['date'], $currencyUsd[2]['buy']],
                [$currencyUsd[3]['date'], $currencyUsd[3]['buy']],
                [$currencyUsd[4]['date'], $currencyUsd[4]['buy']],
            ],
        ],
        [
            'name' => 'EUR',
            'data' => [
                [$currencyEur[0]['date'], $currencyEur[0]['buy']],
                [$currencyEur[1]['date'], $currencyEur[1]['buy']],
                [$currencyEur[2]['date'], $currencyEur[2]['buy']],
                [$currencyEur[3]['date'], $currencyEur[3]['buy']],
                [$currencyEur[4]['date'], $currencyEur[4]['buy']],
            ],
        ],
        [
            'name' => 'AUD',
            'data' => [
                [$currencyAud[0]['date'], $currencyAud[0]['buy']],
                [$currencyAud[1]['date'], $currencyAud[1]['buy']],
                [$currencyAud[2]['date'], $currencyAud[2]['buy']],
                [$currencyAud[3]['date'], $currencyAud[3]['buy']],
                [$currencyAud[4]['date'], $currencyAud[4]['buy']],
            ],
        ],
    ];

    echo \onmotion\apexcharts\ApexchartsWidget::widget([
        'type' => 'area', // default area
        'height' => '350', // default 350
        'width' => '100%', // default 100%
        'chartOptions' => [
            'chart' => [
                'toolbar' => [
                    'show' => true,
                    'autoSelected' => 'zoom'
                ],
            ],
            'xaxis' => [
                'type' => 'datetime',
                // 'categories' => $categories,
            ],
            'plotOptions' => [
                'bar' => [
                    'horizontal' => false,
                    'endingShape' => 'rounded'
                ],
            ],
            'dataLabels' => [
                'enabled' => false
            ],
            'stroke' => [
                'show' => true,
                'colors' => ['transparent']
            ],
            'legend' => [
                'verticalAlign' => 'bottom',
                'horizontalAlign' => 'left',
            ],
        ],
        'series' => $series
    ]);
}
catch (\yii\db\Exception $ex){
    return "Ошибка получения данных";
}
?>

